/**
 * Created by kylemantesso on 1/16/17.
 */

angular.module('nablabsSupportButtonApp')
  .controller('TicketController', function ($firebaseObject, $state, $stateParams, $mdToast, $mdDialog) {
    "ngInject";

    var ctrl = this;

    ctrl.ticket = $firebaseObject(firebase.database().ref().child('tickets').child($stateParams.id));

    ctrl.ticket.$loaded().then(function() {

    });

    ctrl.archive = function(ev) {
      var confirm = $mdDialog.confirm()
        .title('Archive ticket')
        .textContent('Are you sure you want to archive this ticket?')
        .ariaLabel('Archive')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function() {
        ctrl.ticket.$remove();
        $state.go('home.tickets');
        $mdToast.showSimple('Ticket archived');
      }, function() {

      });
    };

    ctrl.addComment = function(ev) {

      if(!ctrl.ticket.comments) {
        ctrl.ticket.comments = {};
      }

      ctrl.ticket.comments[new Date().getTime()] = {
        date: new Date().getTime(),
        notes: ctrl.comment,
        user: 'Bob Smith'
      };

      ctrl.ticket.$save();

      ctrl.comment = '';
      $mdToast.showSimple('Comment added');

    };


  });
