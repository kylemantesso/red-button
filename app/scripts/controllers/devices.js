/**
 * Created by kylemantesso on 1/16/17.
 */

angular.module('nablabsSupportButtonApp')
  .controller('DevicesController', function ($firebaseArray, $mdDialog) {
    "ngInject";

    var ctrl = this;

    ctrl.limit = 10;
    ctrl.page = 1;
    ctrl.orderBy = '-date';

    ctrl.devices = $firebaseArray(firebase.database().ref().child('devices'));

    ctrl.devices.$loaded().then(function() {
      console.log(ctrl.devices);

    });

    ctrl.addDevice = function(ev) {
        $mdDialog.show({
          controller: 'AddDeviceController',
          bindToController: true,
          controllerAs: 'ctrl',
          templateUrl: 'views/add-device.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: true
        })
          .then(function(user) {
            //saveUser(user);
          });
      };

  });
