/**
 * Created by kylemantesso on 1/16/17.
 */

angular.module('nablabsSupportButtonApp')
  .controller('TicketsController', function ($firebaseArray) {
    "ngInject";

    var ctrl = this;

    ctrl.limit = 10;
    ctrl.page = 1;
    ctrl.orderBy = '-date';

    ctrl.tickets = $firebaseArray(firebase.database().ref().child('tickets'));

    ctrl.tickets.$loaded().then(function() {
      console.log(ctrl.tickets);

    });

  });
