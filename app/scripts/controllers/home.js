/**
 * Created by kylemantesso on 1/16/17.
 */

angular.module('nablabsSupportButtonApp')
  .controller('HomeController', function ($scope, $state) {
    "ngInject";

    var ctrl = this;

    $scope.$on('$stateChangeSuccess', function() {
      ctrl.title = $state.current.data.title;
    });

    ctrl.menu = [
      {
        link: '.tickets',
        icon: 'receipt',
        title: 'Tickets'
      },
      {
        link: '.devices',
        icon: 'router',
        title: 'Devices'
      },
      {
        link: '.dashboard',
        icon: 'dashboard',
        title: 'Dashboard'
      }
    ];

  });
