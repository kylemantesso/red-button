'use strict';

angular.module('nablabsSupportButtonApp')
  .controller('DashboardController', function ($firebaseArray) {
    "ngInject";

    var ctrl = this;

    ctrl.tickets = $firebaseArray(firebase.database().ref().child('tickets'));

    ctrl.labels = ["Unallocated", "Active", "Closed", "Unsolved"];
    ctrl.data = [300, 500, 100, 200];

    ctrl.chartOptions = {
      legend: {
        display: true,
        position: 'bottom'
      }
    };

    ctrl.ticketByBranchChart = {};
    ctrl.ticketByBranchChart.labels = ["Collins Street", "Bourke Street", "Flinders Lane", "Elizabeth Stret", "Queen Street"];
    ctrl.ticketByBranchChart.data = [300, 500, 100, 400, 200];

    ctrl.ticketByBranchChart.chartOptions = {
      legend: {
        display: true,
        position: 'bottom'
      }
    };

    ctrl.ticketByTechnician = [];

    ctrl.ticketByProductChart = {};
    ctrl.ticketByProductChart.labels = ["Printer", "Phone", "Cash Counter", "Other"];
    ctrl.ticketByProductChart.data = [300, 500, 100, 400, 200];

    ctrl.ticketByProductChart.chartOptions = {
      legend: {
        display: true,
        position: 'bottom'
      }
    };

    ctrl.technician = {};

    let statusOpen = "Open";
    let statusClosed = "Closed";
    let statusUnassigned = "Unassigned";
    let statusUnsolvable = "Unsolvable";

    ctrl.tickets.$loaded().then(function() {
      console.log("Ticket JSON");

      angular.forEach(ctrl.tickets, function(ticket, ticketKey) {
        var assignee = ticket["assignee"];

        if (!assignee) {
          assignee = "Unassigned";
        }

        if (!("assignee" in ctrl.technician)) {
          ctrl.technician[assignee] = {};
          ctrl.technician[assignee].name = assignee;
          ctrl.technician[assignee][statusOpen] = 0;
          ctrl.technician[assignee][statusClosed] = 0;
          ctrl.technician[assignee][statusUnassigned] = 0;
          ctrl.technician[assignee][statusUnsolvable] = 0;
        }

        ctrl.technician[assignee]["Open"]++;

        console.log(ctrl.technician);

      }, ctrl);

      //assignee: "Bob Smith", branchName: "330 Collins St", clicks: 1, date: 1485216000021, device: {id: "PRINTER_ID_001", type: "Printer"},

      //console.log(ticketJSON);
      //ctrl.chartParams.data = [1,2,5,10]
      //ctrl.labels = ["James"];
      //ctrl.data = [1,2,5,10]
      //console.log("ChartParams");
      //console.log(ctrl.data);
      //$scope.chartParams.series.push([1,2,5,10]);
    });


    /*
    $scope.chartParams = {
      series : [],
      labels : [],
      data : [],

      options : {
        elements: {
          line: {
            fill: false,
            tension: 0.0
          }
          //,
          //point: {
          //  radius: 0
          //}
        },

        showLines: true,
        hover: {
          mode: 'nearest'
        },
        legend: {
          display: true,
          position: 'right'
        },
        responsive: true,
        scales: {
          yAxes: [
            {
              type: 'linear',
              scaleStepWidth: 100,
              ticks: {
                maxTicksLimit: 10,
                //stepSize: 10,
                beginAtZero: false,
                callback: function(value, index, values) {
                  return value.toLocaleString("en-US",{style:"currency", currency:"USD"});
                  //if(parseInt(value) > 10){
                  //  return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  //} else {
                  //  return '$' + value;
                  //}
                },
              },
              id: 'y-axis-1',
              display: true,
              position: 'left'
            }
          ],
          xAxes: [
            {
              type: 'time',
              time: {
                unit: 'minute',
                displayFormats: {
                  minute: 'ddd, h:mmA'
                }
              },
              ticks: {
                mirror: true,
                maxTicksLimit: 3,
                unit: 10
              }
            }
          ]
        }
      }
    };
    */

  });
