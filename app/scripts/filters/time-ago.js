/**
 * Created by kylemantesso on 1/19/17.
 */
angular.module('nablabsSupportButtonApp')
.filter('timeAgo', ['$interval', function ($interval){
  // trigger digest every 60 seconds
  $interval(function (){}, 60000);

  function fromNowFilter(time){
    return moment(time).fromNow();
  }

  fromNowFilter.$stateful = true;
  return fromNowFilter;
}]);
