/**
 * Created by kylemantesso on 1/18/17.
 */
angular.module('nablabsSupportButtonApp')
  .filter('dateRange', function () {
    return function (items, startDate) {
      var retArray = [];

      if (!startDate) {
        return items;
      }

      angular.forEach(items, function (obj) {
        var checkinDate = obj.date;
        if (moment(checkinDate).isAfter(startDate)) {
          retArray.push(obj);
        }
      });

      return retArray;

    }
  });
