'use strict';

angular.module('nablabsSupportButtonApp').service('ReferenceData',
    function () {

      this.getSectors = function() {
        return [
          'Small business (turnover <$5m)',
          'NAB Business (turnover >$5m)',
          'Agribusiness',
          'Health',
          'Government / Education / Community'
        ];
      };

      this.getGenders = function() {
        return [
          'Male',
          'Female'
        ];
      };

      this.getNumberOfGuests = function() {
        return [1,2,3,4,5,6,7,8,9,10];
      };

      this.getReasonsforVisit = function() {
        return [
          'Coworking',
          'Utilising a meeting space',
          'Attending an event',
          'Just dropping in',
          'Meeting / networking with peers'
        ];
      }

      this.getStates = function() {
        return [
          'VIC',
          'NSW',
          'QLD',
          'WA',
          'SA',
          'TAS',
          'NT',
          'ACT'
        ];
      };

      this.getReferalMethod = function() {
        return [
          'Word of mouth',
          'Media',
          'NAB Banker / Employee',
          'Search engine',
          'Prior visit to The Village'
        ];
      };

    });
