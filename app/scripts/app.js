'use strict';

/**
 * @ngdoc overview
 * @name nablabsSupportButtonApp
 * @description
 * # nablabsSupportButtonApp
 *
 * Main module of the application.
 */
angular
  .module('nablabsSupportButtonApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMessages',
    'firebase',
    'md.data.table',
    'ngMaterial',
    'ui.router',
    'md.data.table',
    'LocalStorageModule',
    'chart.js'
  ])
.run(["$rootScope", "$state", function($rootScope, $state) {
  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireSignIn promise is rejected
    // and redirect the user back to the home page
    if (error === "AUTH_REQUIRED") {
      $state.go("login");
    }
  });
}])
  .config(function ($stateProvider, $urlRouterProvider, $mdThemingProvider, $locationProvider, localStorageServiceProvider) {

    localStorageServiceProvider
      .setPrefix('villageApp');

      $urlRouterProvider.otherwise("/");
      $stateProvider
        .state('home', {
          url: "/",
          templateUrl: 'views/home.html',
          controller: 'HomeController as ctrl',
          data: {
            title: 'Home'
          }
        })
        .state('home.devices', {
          url: "devices",
          templateUrl: 'views/devices.html',
          controller: 'DevicesController as ctrl',
          data: {
            title: 'Devices'
          }
        })
        .state('home.dashboard', {
          url: "dashboard",
          templateUrl: 'views/dashboard.html',
          controller: 'DashboardController as ctrl',
          data: {
            title: 'Dashboard'
          }
        })
        .state('home.tickets', {
          url: "tickets",
          templateUrl: 'views/tickets.html',
          controller: 'TicketsController as ctrl',
          data: {
            title: 'Tickets'
          }
        }).state('home.ticket', {
        url: "ticket/:id",
        templateUrl: 'views/ticket.html',
        controller: 'TicketController as ctrl',
        data: {
          title: 'Ticket'
        }
      });


    $mdThemingProvider.definePalette('mcgpalette0', {
      '50': '#ffccc8',
      '100': '#ff857c',
      '200': '#ff5044',
      '300': '#fb1100',
      '400': '#dd0f00',
      '500': '#be0d00',
      '600': '#9f0b00',
      '700': '#810900',
      '800': '#620700',
      '900': '#440500',
      'A100': '#ffccc8',
      'A200': '#ff857c',
      'A400': '#dd0f00',
      'A700': '#810900',
      'hue-1': '#fff',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': '50 100 200 A100 A200'
    });

    $mdThemingProvider.theme('dark-on-light')
      .primaryPalette('mcgpalette0')
      .dark();

    $mdThemingProvider.theme('default')
      .primaryPalette('mcgpalette0')
      .accentPalette('red');

    $locationProvider
      .hashPrefix('');
  });

angular.module('nablabsSupportButtonApp').factory("Auth", ["$firebaseAuth",
  function($firebaseAuth) {
    return $firebaseAuth();
  }
]);

Array.prototype.shuffle = function() {
  var input = this;

  for (var i = input.length-1; i >=0; i--) {

    var randomIndex = Math.floor(Math.random()*(i+1));
    var itemAtIndex = input[randomIndex];

    input[randomIndex] = input[i];
    input[i] = itemAtIndex;
  }
  return input;
};

Array.prototype.remove = function() {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

Array.prototype.inArray = function(comparer) {
  for(var i=0; i < this.length; i++) {
    if(comparer(this[i])) return true;
  }
  return false;
};

// adds an element to the array if it does not already exist using a comparer
// function
Array.prototype.pushIfNotExist = function(element, comparer) {
  if (!this.inArray(comparer)) {
    this.push(element);
  }
};
